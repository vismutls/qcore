<?php
if(!defined("IN_SYSTEM"))
	die('Direct Access Denied!');
	
class Routing extends Engine
{
	static protected $oInstance = NULL;
	
	static public function getInstance($registry)
    {
        if (is_null(self::$oInstance))
        {
            self::$oInstance = new Routing($registry);
        }
        
        return self::$oInstance;
    }
	
    private function __construct($registry) 
	{
		$this->registry = $registry;
		
		if($this->registry['site']['sys']['routing'] == 'MVC')
			$this->route_mvc();
		else
			$this->route_modules();
    }
	
	public function __destruct()
	{
	
	}
	
	private function __clone()
    {
    }
	
	private function get_controller(&$file, &$controller, &$action, &$args) 
	{
		$route = (empty($_GET['route'])) ? '' : $_GET['route'];

		if (empty($route)) 
			$route = 'index';

		$route = trim($route, '/\\');

		$parts = explode('/', $route);

		$cmd_path = APPDIR . DIRSEP . 'controllers' . DIRSEP;

		foreach ($parts as $part) 
		{

			$fullpath = $cmd_path . $part;

			if(is_dir($fullpath)) 
			{
				$cmd_path .= $part . DIRSEP;

				array_shift($parts);

				continue;
			}

			if(is_file($fullpath . '.php')) 
			{
				$controller = $part;

				array_shift($parts);

				break;
			}

		}


		if(empty($controller)) 
			$controller = 'index';

		$action = array_shift($parts);

		if (empty($action)) 
			$action = 'index';


		$file = $cmd_path . $controller . '.php';

		$args = $parts;

	}
	
	private function route_mvc() 
	{	
		$this->get_controller($file, $controller, $action, $args);

		if (is_readable($file) == false) 
		{
				die ('Controller' . $file . 'required, but not found!');
		}

		require_once($file);

		$class = 'Controller_' . $controller;
		
		$controller = new $class($this->registry);

		if (is_callable(array($controller, $action)) == false) 
		{
				die ('Controller' . $file . 'required, but not found!');
		}

		$controller->$action();
	}
	
	
	private function route_modules()
	{
	
	}

}