<?php
if(!defined("IN_SYSTEM"))
	die('Direct Access Denied!');

Abstract Class Controller extends Engine
{
	public function __construct($registry) 
	{
		$this->registry = $registry;
		$this->config 	= Config::getInstance($this->registry);
		$this->cache 	= Cache::getInstance($this->registry);
		$this->db 		= Database::getInstance($this->registry);
		$this->tpl 		= Template::getInstance($this->registry);
	}
	
	public function __destruct()
	{
	}
	
	abstract function index();
}