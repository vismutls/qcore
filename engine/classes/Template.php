<?
if(!defined("IN_SYSTEM"))
	die('Direct Access Denied!');
	
class Template extends Engine
{
	static protected $oInstance = NULL;

	static public function getInstance($registry)
    {
        if (is_null(self::$oInstance))
        {
            self::$oInstance = new Template($registry);
        }
        
        return self::$oInstance;
    }

    private function __construct($registry) 
	{
		$this->registry = $registry;
    }
	
	private function __clone()
    {
    }
	
	public function view($file)
	{
		if(isset($file))
		{
			$file_exp = explode('.', $file);
			if(empty($file_exp[1]))
				$file = $file . '.php';
			else
				$file = $file;
			
			$path = TPLDIR . DIRSEP . $file;
			
			if(file_exists($path))
				require_once($path);
			else
				trigger_error('Template ' . $file . ' (' . $path . ') required, but not found!');
		}
		else
		{
			trigger_error('You must set one string parameter, example: index.tpl');
		}
	}
}