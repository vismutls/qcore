<?php
if(!defined("IN_SYSTEM"))
	die('Direct Access Denied!');

/*
 * Автозагрузка классов
 */
function __autoload($class)
{
	if(isset($class))
	{
		if($class != 'finfo')
		{
			if(file_exists(SYSDIR.'/classes/'.$class.'.php'))			
				require_once(SYSDIR.'/classes/'.$class.'.php');
			else
				Engine::getInstance()->error('Class "'.$class.'" required, but not found');
		}
	}
}	
	
class Engine 
{
	public $oEngine				= NULL;
	public $config				= NULL;
	public $routing				= NULL;
	public $cache				= NULL;
	public $db					= NULL;
	public $tpl					= NULL;
	public $registry			= NULL;
    static protected $oInstance = NULL;
	
	/**
	 * Конструктор класса
	 */
    private function __construct() 
	{
		static $already_loaded;
		$USE_GZ	= true;
		
        if (extension_loaded('zlib') && ini_get('zlib.output_compression') != '1' && ini_get('output_handler') != 'ob_gzhandler' && $USE_GZ && !$already_loaded) {
            @ob_start('ob_gzhandler');
        } elseif (!$already_loaded)
            @ob_start();
        $already_loaded = true;
		
		$this->Init();

		$oEngine = $this;
	}
	
	/**
	 * Деструктор класса
	 */
	public function __destruct()
	{
		$oEngine	= NULL;
		$config		= NULL;
		$routing	= NULL;
		$cache		= NULL;
		$db			= NULL;
		$tpl		= NULL;
		$registry	= NULL;
		$oInstance	= NULL;
	}
	
	/**
	 * Инициализация классов
	 */
	protected function Init()
	{
		$registry 	= new Registry;
		$config		= Config::getInstance($registry);
		$cache		= Cache::getInstance($registry);
		$db			= Database::getInstance($registry);
		$router		= Routing::getInstance($registry);
		$tpl		= Template::getInstance($registry);
		
		$this->registry	= $registry;
		$this->config	= $config;
		$this->cache	= $cache;
		$this->db		= $db;
		$this->router	= $router;
		$this->tpl		= $tpl;
	}
	
	public function error($info = NULL, $level = E_ALL) 
    {
		switch($level) {
			case E_ERROR:
			case E_ALL:
			case E_WARNING:
				die($info);
			break;
		}
		die($info);
	}
    
    static public function getInstance() 
	{
		if (isset(self::$oInstance) and (self::$oInstance instanceof self)) 
		{
			return self::$oInstance;
		}
		else 
		{
			self::$oInstance = new self();
			return(self::$oInstance);
		}
	}
    
    public function Unload() 
	{
		unset($this->registry);
		unset($this->config);
		unset($this->cache);
		unset($this->db);
		unset($this->router);
		unset($this->tpl);
		unset($this);
		
		@ob_end_flush();
	}
    
    public function timer() 
    {
		list($usec, $sec) = explode(" ", microtime());
		
		return ((float)$usec + (float)$sec);
	}

}	
?>