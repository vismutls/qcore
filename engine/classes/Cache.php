<?php
if(!defined("IN_SYSTEM"))
	die('Direct Access Denied!');
   
Class Cache Extends Engine
{
	var $used						= FALSE;
	static protected $oInstance		= NULL;
	
	static public function getInstance($registry)
    {
        if (is_null(self::$oInstance))
        {
            self::$oInstance = new Cache($registry);
        }
        
        return self::$oInstance;
    }
	
	private function __construct($registry)
	{
		$this->registry = $registry;
		
		$this->Init($registry);
	}
	
	public function __destruct()
	{
		$used		= NULL;
		$oInstance	= NULL;
	}
	
	private function __clone()
    {
    }
	
	public function Init()
	{
		if($this->registry['site']['sys']['cache_enable']) 
		{
			switch($this->registry['site']['sys']['cache_type']) 
			{
				case 'XCache':
					$cache = new Cache_xcache();
					break;
				case 'eAccelerator':
					$cache = new Cache_eaccelerator();
					break;
				default:
					$cache = $this;
			}
		}
		else
		{
			$cache = $this;
		}
	}
	
	/**
	 * Возвращаем значение переменной
	 */
	function get($name) {
		return false;
	}
	
	/**
	 * Записываем значение переменной
	 */
	function set($name, $value, $ttl = 0) {
		return false;
	}
	
	/**
	 * Удаляем переменную
	 */
	function rm($name) {
		return false;
	}
}

class Cache_xcache extends Cache 
{
    var $used = true;

    function Cache_xcache() {
        if (!$this->is_installed()) {
            die('Error: XCache extension not installed');
        }
    }

    function get($name) {
        return unserialize(xcache_get($name));
    }

    function set($name, $value, $ttl = 0) {
        return xcache_set($name, serialize($value), $ttl);
    }

    function exists($name) {
        return (bool) xcache_isset($name) && $_GET["no_cache"] != 1 && !defined('NO_CACHE');
    }

    function rm($name) {
        return xcache_unset($name);
    }

    function is_installed() {
        return function_exists('xcache_get');
    }
}

class Cache_eaccelerator extends Cache 
{
    var $used = true;

    function Cache_eaccelerator() {
        if (!$this->is_installed()) {
            die('Error: eAccelerator extension not installed');
        }
    }

    function get($name) {
        return unserialize(eaccelerator_get($name));
    }

    function set($name, $value, $ttl = 0) {
        return eaccelerator_put($name, serialize($value), $ttl);
    }

    function exists($name) {
        return eaccelerator_get($name) && $_GET["no_cache"] != 1 && !defined('NO_CACHE');
    }

    function rm($name) {
        return eaccelerator_rm($name);
    }

    function is_installed() {
        return function_exists('eaccelerator_get');
    }
}
?>