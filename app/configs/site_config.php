<?php
if(!defined("IN_SYSTEM"))
	exit('Direct Access Denied!');

define('PATH', 'http://'.$_SERVER['SERVER_NAME'].'/');

$appConfig['USE_GZ'] = TRUE;

$appConfig['sys'] = array
(
	'sitename'		=>	'',
	'version'		=>	'0.2',
	'cache_enable'	=>	FALSE,
	'cache_type'	=>	'XCache',
	'routing'		=>	'MVC' // MVC, Modules
);

$appConfig['meta'] = array
(
	'keywords'		=>	'',
	'description'	=>	''
);