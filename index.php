<?php
/**
 * qCore 0.2
 * 
 * @author	Oleg "rcount"
 * @git		http://bitbucket.org/rcount/qcore
 */

/**
 * Определение среды разработки
 *
 * development - статус "в разработке". будут отображаться все ошибки
 * testing, production - статус тестирования, или открытой работы - ошибки не отображатся.
 */
define('ENVIRONMENT', 'development');

if (defined('ENVIRONMENT'))
{
	switch (ENVIRONMENT)
	{
		case 'development':
			error_reporting(E_ALL);
		break;
	
		case 'testing':
		case 'production':
			error_reporting(0);
		break;

		default:
			exit('The application environment is not set correctly.');
	}
}

/**
 * Название системной папки
 */
$system_folder = 'engine';

/**
 * Название папки приложения
 */
$application_folder = 'app';

/**
 * Название папки шаблонов и файлов отображеня
 */
$templates_folder = 'views';

/**
 * Определение констант папок
 */
define('DIRSEP', DIRECTORY_SEPARATOR);

define('ROOT_DIR', dirname(dirname(__FILE__)) . '/www/');

define('SYSDIR', ROOT_DIR . DIRSEP . $system_folder);

define('APPDIR', ROOT_DIR . DIRSEP . $application_folder);

define('TPLDIR', APPDIR . DIRSEP . $templates_folder);

define('IN_SYSTEM', true);

/* *
 * Инициализируем ядро
 */
require_once(SYSDIR.'/classes/Engine.php');

$oEngine	= Engine::getInstance();

$tstart		= $oEngine->timer();

$oEngine->Unload();